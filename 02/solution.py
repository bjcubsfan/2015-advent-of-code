#!/usr/bin/env python

def area_of_smallest_side(length, width, height):
    return min(((length * width), (width * height), (height * length)))

def volume(length, width, height):
    return length * width * height

def area_needed(sides):
    length, width, height = sides
    surface_area = 2 * length * width + 2 * width * height + 2 * height * length
    smallest_side = area_of_smallest_side(length, width, height)
    return surface_area + smallest_side

def ribbon_needed(sides):
    length, width, height = sides
    small_sides = sides[:]
    small_sides.remove(max(sides))
    smallest_face = 0
    for side in small_sides:
        smallest_face += side + side
    return smallest_face + volume(length, width, height)


total_square_feet_needed = 0
total_feet_of_ribbon_needed = 0
with open('./input') as input_file:
    for line in input_file:
        sides = line.split('x')
        sides = list(map(int, sides))
        total_square_feet_needed += area_needed(sides)
        total_feet_of_ribbon_needed += ribbon_needed(sides)

print('The elves need {} square feet of wrapping paper.'.format(total_square_feet_needed))
print('The elves need {} feet of ribbon.'.format(total_feet_of_ribbon_needed))

