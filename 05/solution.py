#!/usr/bin/env python3

from itertools import zip_longest


BAD_STRINGS = ['ab', 'cd', 'pq', 'xy']
VOWELS = 'aeiou'
DEBUG = False
TEST_STRINGS = [('ugknbfddgicrmopn', True),
    ('aaa', True),
    ('aaxa', True),
    ('aeidd', True),
    ('aeicdd', False),
    ('jchzalrnumimnmhp', False),
    ('haegwjzuvuyypxyu', False),
    ('dvszwmarrgswjxmb', False),
    ('aaab', False),
    ('aaacd', False),
    ('aaapq', False),
    ('aaaxy', False),
    ]
TRUE_NICE_TEST_STRINGS = [('qjhvhtzxzqqjkmpb', True),
    ('xxyxx', True),
    ('uurcxstgmygtbstg', False),
    ('ieodomkazucvgmuy', False),
    ]


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def has_three_vowels(string):
    has_three = False
    num_vowels = 0
    for char in string:
        if char in VOWELS:
            num_vowels += 1
    if num_vowels >= 3:
        has_three = True
    if DEBUG:
        print('has_three_vowels', has_three, num_vowels)
    return has_three


def has_a_double_letter(string):
    has_double = False
    prev_char = string[0]
    rest_of_letters = string[1:]
    double = None
    for char in rest_of_letters:
        if prev_char == char:
            has_double = True
            double = prev_char + char
            break
        else:
            prev_char = char
    if DEBUG:
        print('has_a_double_letter', has_double, double)
    return has_double


def has_no_bad_strings(string):
    has_no_bad_strings = True
    has_this_one = None
    for bad_string in BAD_STRINGS:
        if bad_string in string:
            has_this_one = bad_string
            has_no_bad_strings = False
            break
    if DEBUG:
        print('has_no_bad_strings', has_no_bad_strings, has_this_one)
    return has_no_bad_strings


def string_is_nice(string):
    if DEBUG:
        print()
        print(string.strip())
    has_3v = has_three_vowels(string)
    doub = has_a_double_letter(string)
    no_bad = has_no_bad_strings(string)
    is_nice = doub and no_bad and has_3v
    if DEBUG:
        print('is_nice', is_nice)
    return is_nice


def has_pair_twice(string):
    has_pair_twice = False
    pairs = []
    winning_pair = None
    for first, second in grouper(string, 2, '-'):
        pairs.append(first + second)
    for first, second in grouper(string[1:], 2, '-'):
        pairs.append(first + second)
    for pair in pairs:
        if string.count(pair) >= 2:
            has_pair_twice = True
            winning_pair = pair
            break
    if DEBUG:
        print('has_pair_twice', has_pair_twice, winning_pair)
    return has_pair_twice


def has_repeat_with_between(string):
    has_repeat_with_between = False
    winning_three = None
    threesomes = []
    for three in grouper(string, 3, '-'):
        threesomes.append(''.join(three))
    for three in grouper(string[1:], 3, '-'):
        threesomes.append(''.join(three))
    for three in grouper(string[2:], 3, '-'):
        threesomes.append(''.join(three))
    for three in threesomes:
#         if 'odo' in string:
#             import IPython; IPython.embed()
        if three[0] == three[2]:
            has_repeat_with_between = True
            winning_three = three
            break
    if DEBUG:
        print('has_repeat_with_between', has_repeat_with_between, winning_three)
    return has_repeat_with_between


def string_is_truly_nice(string):
    if DEBUG:
        print()
        print(string.strip())
    pair_twice = has_pair_twice(string)
    one_repeat_btwn = has_repeat_with_between(string)
    truly_nice = pair_twice and one_repeat_btwn
    if DEBUG:
        print('truly_nice', truly_nice)
    return truly_nice


if DEBUG:
    print('********************Starting TEST_STRINGS********************')
for string, truth in TEST_STRINGS:
    assert(string_is_nice(string) == truth)
for string, truth in TRUE_NICE_TEST_STRINGS:
    assert(string_is_truly_nice(string) == truth)
if DEBUG:
    print('********************Finishing TEST_STRINGS********************')
nice_strings = 0
naughty_strings = 0
truly_nice_strings = 0
truly_naughty_strings = 0
with open('./input') as input_file:
    for index, string in enumerate(input_file):
        if DEBUG:
            break
        if string_is_nice(string):
            nice_strings += 1
        else:
            naughty_strings += 1
        if string_is_truly_nice(string):
            truly_nice_strings += 1
        else:
            truly_naughty_strings += 1
print('5.1: There are {} nice strings and {} naughty strings'.format(
    nice_strings, naughty_strings))
print('5.2: There are {} nice strings and {} naughty strings'.format(
    truly_nice_strings, truly_naughty_strings))

