#!/usr/bin/env python3

from pprint import pprint

from numpy import uint16

TEST_INPUT = '''123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i
'''

TEST_SIGNALS = {
    'd': 72,
    'e': 507,
    'f': 492,
    'g': 114,
    'h': 65412,
    'i': 65079,
    'x': 123,
    'y': 456,
    }

COMMANDS = [
    '->',
    'AND',
    'OR',
    'LSHIFT',
    'RSHIFT',
    'NOT',
    ]


def get_symbols(input_instructions):
    wire_names = {}
    for line in input_instructions:
        for symbol in line.split():
            if symbol.isdigit():
                pass
            elif symbol in COMMANDS:
                pass
            else: 
                wire_names[symbol] = None
    return wire_names


def solve_equation(symbols, equation):
    "Find result for the given equation"
    orig_parts = equation.split(' ')
    parts = [part.strip() for part in orig_parts]
    if parts[0] == 'NOT':
        value_to_flip = symbols[parts[1]]
        if value_to_flip:
            return ~ value_to_flip
    elif len(parts) == 1 and parts[0].isdigit():
        return uint16(parts[0])
    elif len(parts) == 3 and symbols[parts[0]] and parts[2].isdigit():
        shift_amount = uint16(parts[2])
        if parts[1] == 'RSHIFT':
            return symbols[parts[0]] >> shift_amount
        elif parts[1] == 'LSHIFT':
            return symbols[parts[0]] << shift_amount
    elif len(parts) == 3 and symbols[parts[0]] and symbols[parts[2]]:
        if parts[1] == 'AND':
            return symbols[parts[0]] & symbols[parts[2]]
        elif parts[1] == 'OR':
            return symbols[parts[0]] | symbols[parts[2]]
    return None


def solve_possible_signals(input_text, symbols):
    """Go through signals once, solving where possible."""
    next_symbol_iteration = symbols.copy()
    for line in input_text:
        equation, store_to = line.split('->')
        result = solve_equation(next_symbol_iteration, equation.strip())
        if result:
            next_symbol_iteration[store_to.strip()] = result
    return next_symbol_iteration


def are_there_unsolved(symbols):
    for symbol in symbols:
        if not symbols[symbol]:
            if __debug__:
                print('****************** more symbols **********************')
                pprint(symbols)
            return True
    if __debug__:
        print('############################## ALL DONE ##############################')
        pprint(symbols)
    return False


def solve_signals(input_text):
    unsolved_inputs = True
    symbols = get_symbols(input_text)
    if __debug__:
        pprint(symbols)
    while unsolved_inputs:
        symbols = solve_possible_signals(input_text, symbols)
        unsolved_inputs = are_there_unsolved(symbols)
    return symbols


test_input = TEST_INPUT.strip().split('\n')
solved = solve_signals(test_input)
assert solved == TEST_SIGNALS

real_input = open('./input').readlines()
solved = solve_signals(real_input)
print('the signal provided to wire a is {}'.format(solved['a']))

