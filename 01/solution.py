#!/usr/bin/env python

floor = 0
had_negative = False
with open('./input') as input_file:
    for line in input_file:
        for index, char in enumerate(line):
            if char == '(':
                floor += 1
            elif char == ')':
                floor -= 1
            else:
                continue
            if floor < 0 and not had_negative:
                print('Santa is negative at index {}'.format(index + 1))
                had_negative = True
print('Santa is at floor {}'.format(floor))
