#!/usr/bin/env python3

import sys

TEST_INSTRUCTIONS = [
    'turn on 0,0 through 999,999',
    'toggle 0,0 through 999,0',
    'turn off 499,499 through 500,500',
    ]

def generate_pairs(coordinates):
    pairs = []
    for row in range(coordinates[0][0], coordinates[1][0] + 1):
        for column in range(coordinates[0][1], coordinates[1][1] + 1):
            pairs.append((row, column))
    if __debug__:
        print('number of lights', len(pairs))
    return pairs


def turn_on(pair, lights):
    lights[pair[0]][pair[1]] += 1


def turn_off(pair, lights):
    if lights[pair[0]][pair[1]] == 0:
        return
    else:
        lights[pair[0]][pair[1]] -= 1


def toggle(pair, lights):
    lights[pair[0]][pair[1]] += 2

def parse_coordinates(coordinates):
    final_coords = []
    for pair in coordinates:
        final_coords.append(tuple(map(int, pair.split(','))))
    final_coords = list(final_coords)
    if __debug__:
        print('Parsed coords', final_coords)
    return final_coords


def perform(instruction, lights):
    instruction = instruction.split()
    coordinates = []
    for part in instruction:
        if ',' in part:
            coordinates.append(part)
    coordinates = parse_coordinates(coordinates)
    pairs = generate_pairs(coordinates)
    if instruction[1] == 'off':
        todo = turn_off
    elif instruction[1] == 'on':
        todo = turn_on
    elif instruction[0] == 'toggle':
        todo = toggle 
    for pair in pairs:
        todo(pair, lights)

lights = [[0 for x in range(1000)] for x in range(1000)]
if __debug__:
    for test_instruction in TEST_INSTRUCTIONS:
        perform(test_instruction, lights)
    sys.exit(0)
with open('./input') as input_file:
    for instruction in input_file:
        perform(instruction, lights)
total_brightness = 0
for row in lights:
    for light_brighness in row:
        total_brightness += light_brighness
print('There is a total brightness of  {}'.format(total_brightness))

