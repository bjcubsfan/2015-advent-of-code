#!/usr/bin/env python3

from hashlib import md5

no_winner = True
winning_number = 0
given_input = 'yzbqklnj'
while no_winner:
    winning_number += 1
    combined_input = given_input + str(winning_number)
    md5_output = md5(combined_input.encode('ascii')).hexdigest()
    if md5_output[:6] == '000000':
        no_winner = False
print('The number for Santa number is {}, which yields a hash of {} .'.format(winning_number, md5_output))

