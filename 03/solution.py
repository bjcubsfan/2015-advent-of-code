#!/usr/bin/env python

houses_with_presents = set()
santa = 0
robosanta = 1
locations = [[0, 0], [0, 0]]
with open('./input') as input_file:
    for line in input_file:
        houses_with_presents.add(tuple(locations[santa]))
        for index, char in enumerate(line):
            if index % 2 == 0:
                mover = santa
            else:
                mover = robosanta
            if char == '^':
                locations[mover][1] += 1
            elif char == 'v':
                locations[mover][1] -= 1
            elif char == '>':
                locations[mover][0] += 1
            elif char == '<':
                locations[mover][0] -= 1
            houses_with_presents.add(tuple(locations[mover]))
print('Santa and robo delivered presents to {} houses.'.format(len(houses_with_presents)))

